(function () {
    "use strict";
// Declare app level module which depends on views, and components
    var ob = angular.module('offnibuehni', [
        'ui.bootstrap',
        'ui.router',
        'bootstrapLightbox'
    ]);

    ob.controller('GalleryController', ['$scope', '$state', 'eventRepository', 'Lightbox', '$log', function ($scope, $state, eventRepository, Lightbox, $log) {
        $scope.images = [];

        $scope.event = eventRepository.getEventById(parseInt($state.params.eventID, 10));

        if ($scope.event.bilder) {
            for (var i = 0; i < $scope.event.bilder.length; i++) {
                $scope.images.push({
                    url: $scope.event.bilder[i].fullsize,
                    thumbUrl: $scope.event.bilder[i].thumbnail
                });
            }
        }

        $scope.openLightboxModal = function (index) {
            Lightbox.openModal($scope.images, index);
        };
    }]);
    ob.controller('TeamController', ['$scope', function ($scope) {
        $scope.mitglieder = [
            {
                name: 'Juri Dossenbach',
                funktionen: ['Präsident'],
                bild: 'img/portraits/juri.jpg'
            },
            {
                name: 'Leo Butie',
                funktionen: ['Kassier', 'Tontechniker'],
                bild: 'img/portraits/leo.jpg'
            },
            {
                name: 'Simon Erni',
                funktionen: ['Barverantwortlicher', 'Lichttechniker', 'Websiteentwicklung'],
                bild: 'img/portraits/simon.jpg'
            },
            {
                name: 'Angela Addo',
                funktionen: ['Programmverantwortliche'],
                bild: 'img/portraits/angie.jpg'
            },
            {
                name: 'Elio Donauer',
                funktionen: ['Tontechniker'],
                bild: 'img/portraits/elio.jpg'
            },
            {
                name: 'Keegan Jornot',
                funktionen: ['PR Verantwortlicher'],
                bild: 'img/portraits/keegan.jpg'
            }
        ];
    }]);

    ob.controller('AktuellController', ['$scope', 'eventRepository', '$log', function ($scope, eventRepository, $log) {
        $scope.event = eventRepository.getAktuellerEvent();
    }]);

    ob.controller('EventDetailController', ['$scope', 'eventRepository', '$state', function ($scope, eventRepository, $state) {
        $scope.event = eventRepository.getEventById(parseInt($state.params.eventID), 10);

        if (eventRepository.hasNextEvent($scope.event)) {
            $scope.naechsterEvent = eventRepository.getNextEvent($scope.event);
        }

        if (eventRepository.hasPreviousEvent($scope.event)) {
            $scope.vorherigerEvent = eventRepository.getPreviousEvent($scope.event);
        }

    }]);

    ob.config(function ($stateProvider, $urlRouterProvider) {
        //
        // For any unmatched url, redirect to /state1
        $urlRouterProvider.otherwise("/event/aktuell");
        //
        // Now set up the states
        $stateProvider
            .state('verein', {
                url: "/verein",
                templateUrl: "../views/verein.html"
            })
            .state('goenner', {
                url: "/goenner",
                templateUrl: "../views/gönner.html"
            })
            .state('kontakt', {
                url: "/kontakt",
                templateUrl: "../views/kontakt.html"
            })
            .state('team', {
                url: "/team",
                templateUrl: "../views/team.html",
                controller: 'TeamController'
            })
            .state('anfahrt', {
                url: "/anfahrt",
                templateUrl: "../views/anfahrt.html"
            })
            .state('auftreten', {
                url: "/auftreten",
                templateUrl: "../views/auftreten.html"
            })
            .state('event_aktuell', {
                url: "/event/aktuell",
                templateUrl: "../views/event/aktuell.html",
                controller: 'AktuellController'
            })
            .state('event_detail', {
                url: "/event/detail/:eventID",
                templateUrl: "../views/event/detail.html",
                controller: 'EventDetailController'
            })
            .state('event_jahr', {
                url: "/event/jahr/:jahrID",
                templateUrl: '../views/event/jahr.html',
                controller: 'EventJahrController'
            });
    });

    ob.controller('NavigationController', ['$scope', '$state', 'eventRepository', function ($scope, $state, eventRepository) {
        $scope.mobileMenuCollapsed = true;
        $scope.status = $state;
        $scope.eventYears = eventRepository.getAllYears();

    }]);

    ob.controller('EventJahrController', ['$scope', '$state', '$log', 'eventRepository', function ($scope, $state, $log, eventRepository) {

        $scope.gewaehltesJahr = parseInt($state.params.jahrID, 10);

        if (eventRepository.hasNextYear($scope.gewaehltesJahr)) {
            $scope.naechstesJahr = eventRepository.getNextYear($scope.gewaehltesJahr);
        }

        if (eventRepository.hasPreviousYear($scope.gewaehltesJahr)) {
            $scope.vorherigesJahr = eventRepository.getPreviousYear($scope.gewaehltesJahr);
        }

        $scope.events = eventRepository.getEventsInYear($scope.gewaehltesJahr);

    }]);

    ob.factory('eventRepository', function () {

        return {

            getAllYears: function () {
                return [2014, 2013, 2012, 2011, 2010];
            },
            getEventById: function (id) {
                for (var i = 0; i < this.getAllEvents().length; i++) {
                    if (this.getAllEvents()[i].id === id) {
                        return this.getAllEvents()[i];
                    }
                }
            },
            getAktuellerEvent: function () {
                return this.getAllEvents()[0];
            },
            getEventsInYear: function (year) {
                var returnEvents = [];
                for (var i = 0; i < this.getAllEvents().length; i++) {
                    if (this.getAllEvents()[i].datum.getFullYear() === year) {
                        returnEvents.push(this.getAllEvents()[i]);
                    }
                }
                return returnEvents;
            },
            getAllEvents: function () {
                return [
                    {
                        id: 1,
                        titel: "99. Offni Bühni",
                        datum: new Date('2013-01-07'),
                        slogan: "My Slogan",
                        artists: [{
                            name: "Luc Jornot",
                            kunst: "Gitarre und so"
                        }, {
                            name: "Ursina Gössi",
                            kunst: "All the things"
                        }],
                        flyers: ['img/flyer/2013/01-JANUAR-2013-1.jpg', 'img/flyer/2013/01-JANUAR-2013-2.jpg'],
                        eventtext: 'Eintritt frei - Kollekte',
                        bilder: [
                            {
                                fullsize: 'img/eventpicture/1.jpg',
                                thumbnail: 'img/eventpicture/1.jpg'
                            },
                            {
                                fullsize: 'img/eventpicture/2.jpg',
                                thumbnail: 'img/eventpicture/2.jpg'
                            },
                            {
                                fullsize: 'img/eventpicture/3.jpg',
                                thumbnail: 'img/eventpicture/3.jpg'
                            }
                        ]
                    },
                    {
                        id: 2,
                        titel: "100. Offni Bühni",
                        datum: new Date('2013-01-09'),
                        slogan: "My Slogan",
                        artists: [{
                            name: "Luc Jornot",
                            kunst: "Gitarre und so"
                        }, {
                            name: "Ursina Gössi",
                            kunst: "All the things"
                        }],
                        flyers: ['img/flyer/2013/01-JANUAR-2013-1.jpg'],
                        eventtext: 'Eintritt frei - Kollekte'
                    },
                    {
                        id: 3,
                        titel: "99. Offni Bühni",
                        datum: new Date('2013-01-07'),
                        slogan: "My Slogan",
                        artists: [{
                            name: "Luc Jornot",
                            kunst: "Gitarre und so"
                        }, {
                            name: "Ursina Gössi",
                            kunst: "All the things"
                        }],
                        flyers: ['img/flyer/2013/01-JANUAR-2013-1.jpg'],
                        eventtext: 'Eintritt frei - Kollekte'
                    },
                    {
                        id: 4,
                        titel: "99. Offni Bühni",
                        datum: new Date('2013-01-07'),
                        artists: [{
                            name: "Luc Jornot",
                            kunst: "Gitarre und so"
                        }, {
                            name: "Ursina Gössi",
                            kunst: "All the things"
                        }],
                        flyers: ['img/flyer/2013/01-JANUAR-2013-1.jpg'],
                        eventtext: 'Eintritt frei - Kollekte'
                    },
                    {
                        id: 5,
                        titel: "99. Offni Bühni",
                        datum: new Date('2013-01-07'),
                        slogan: "My Slogan",
                        artists: [{
                            name: "Luc Jornot",
                            kunst: "Gitarre und so"
                        }, {
                            name: "Ursina Gössi",
                            kunst: "All the things"
                        }],
                        flyers: ['img/flyer/2013/01-JANUAR-2013-1.jpg'],
                        eventtext: 'Eintritt frei - Kollekte'
                    },
                    {
                        id: 6,
                        titel: "123. Offni Bühni",
                        datum: new Date('2014-01-06'),
                        slogan: "My Slogan",
                        artists: [{
                            name: "Luc Jornot",
                            kunst: "Gitarre und so"
                        }, {
                            name: "Ursina Gössi",
                            kunst: "All the things"
                        }],
                        flyers: ['img/flyer/2014/01-OFFNI-BUEHNI-2014-JAN-A.jpg'],
                        eventtext: 'Eintritt frei - Kollekte'
                    }
                ];
            },
            hasNextYear: function (year) {
                for (var i = 0; i < this.getAllYears().length; i++) {
                    if (this.getAllYears()[i] === year + 1) {
                        return true;
                    }
                }
                return false;
            },
            getNextYear: function (year) {
                return year + 1;
            },
            getPreviousYear: function (year) {
                return year - 1;
            },
            hasPreviousYear: function (year) {
                for (var i = 0; i < this.getAllYears().length; i++) {
                    if (this.getAllYears()[i] === year - 1) {
                        return true;
                    }
                }
                return false;
            },
            hasNextEvent: function (event) {
                for (var i = 0; i < this.getAllEvents().length; i++) {
                    if (this.getAllEvents()[i].id === event.id) {
                        return i < (this.getAllEvents().length - 1);
                    }
                }
            },
            hasPreviousEvent: function (event) {
                for (var i = 0; i < this.getAllEvents().length; i++) {
                    if (this.getAllEvents()[i].id === event.id) {
                        return i > 0;
                    }
                }
            },
            getNextEvent: function (event) {
                for (var i = 0; i < this.getAllEvents().length; i++) {
                    if (this.getAllEvents()[i].id === event.id) {
                        return this.getAllEvents()[i + 1];
                    }
                }
            },
            getPreviousEvent: function (event) {
                for (var i = 0; i < this.getAllEvents().length; i++) {
                    if (this.getAllEvents()[i].id === event.id) {
                        return this.getAllEvents()[i - 1];
                    }
                }
            }
        };
    });
})();